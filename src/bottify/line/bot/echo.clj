(ns bottify.line.bot.echo
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.set :as s]
            [bottify.line.bot :as bot]
            [bottify.container :refer [boot-on-container]]
            [bottify.tokenizer :as t]
            [clojure.data.json :as json]
            [taoensso.timbre :as log])
  
  (:import [com.linecorp.bot.model.event Event]
           [com.linecorp.bot.model.event.message TextMessageContent ]
           [com.linecorp.bot.model.message TextMessage]))



(defmulti handler (fn [ev] (class (-> ev (.getMessage)))))

(defmethod handler TextMessageContent [event]
  (TextMessage. (-> event (.getMessage) bean :text)))

(defmethod handler :default [event]
  (TextMessage. "Not implemented yet"))

(defn bootstrap [{:keys [host path port] :as bot-opts}]
  (log/debug bot-opts)
  (boot-on-container (bot/build handler bot-opts)
                     {:host (or host "0.0.0.0") :path (or path "/echo") :port (or port 10000)} ))
