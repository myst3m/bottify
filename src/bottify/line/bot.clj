(ns bottify.line.bot
  (:require
   [clojure.java.io :as io]
   [clojure.set :as s]
   [clojure.data.json :as json]
   [clojure.string :as str]  
   [taoensso.timbre :refer :all])
  
  (:import [com.linecorp.bot.client ])
  (:import [com.linecorp.bot.servlet LineBotCallbackRequestParser]
           [com.linecorp.bot.client
            LineSignatureValidator
            LineMessagingServiceBuilder
            LineMessagingService]
           [com.linecorp.bot.model ReplyMessage ]
           [com.linecorp.bot.model.message TextMessage ]
           [com.linecorp.bot.servlet LineBotCallbackRequestParser]
           [java.util ArrayList]))

;;(def ^:dynamic *options* {})
;;(defn options []  *options*)

;;(def ^:dynamic *bot* nil)
;;(def ^:dynamic *validator* nil)
;;(defn bot [] *bot*)
;(defn validator [] *validator*)

(defn config->map [file-name]
  (try
    (json/read-str (slurp file-name) :key-fn keyword)
    (catch Exception e (warn e) {:channel-secret "NOT-SET"
                                 :channel-access-token "NOT-SET"})))

(defn message-process [line-service bot-fn opts {:keys [callback]}]
  (dorun
   (for  [ev (-> callback (.getEvents))]
     (do
       (debug ev)
       (-> line-service
           (.replyMessage (ReplyMessage. (-> ev (.getReplyToken)) (bot-fn ev opts)))
           (.execute)
           (.body)))))
  {:status 200 :body "OK"})


(defn wrap-line-signature-validate [validator handler]
  (fn [request]
    (debug (str request))    
    (let [body (slurp (:body request))
          signature (get-in request [:headers "x-line-signature"])]
      (handler  (assoc request
                       :body body
                       :callback (try (-> (LineBotCallbackRequestParser. validator)
                                          (.handle signature body))
                                      (catch Exception e (println e))))))))

(defn wrap-keywordize-from-json [handler]
  (fn [request]
    (debug (str request))
    (handler (assoc request :body (try
                                    (json/read-str (:body request) :key-fn keyword)
                                    (catch Exception e (warn "No JSON request: " (:remote-addr request))))))))



(defn build [bot-handler {:keys [secrets] :as opts}]
  (if-not secrets
    (error "No specified file for secrets")
    (let [line-config-map (:line (config->map secrets))
          validator (LineSignatureValidator. (.getBytes (:channel-secret line-config-map)))
          line-service (-> (LineMessagingServiceBuilder/create (:channel-access-token line-config-map)) (.build))]
      (info "Secrets:" secrets)
      (info "")
      (debug "Channel-Access-Token:" (:channel-access-token line-config-map))
      (debug "Channel-Secret:" (:channel-secret line-config-map))
      (info "")
      (try
        (bound-fn*
         (-> (partial message-process line-service bot-handler opts) ;; Must use symbol function in macro
             (wrap-keywordize-from-json)
             ((partial wrap-line-signature-validate validator))))
        (catch Exception e# (error e#))))))
