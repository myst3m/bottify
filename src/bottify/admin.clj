(ns bottify.admin
  (:gen-class)
  (:require [bottify.container :refer [boot-on-container]]))

(defn handler [req]
  {:status 200
   :body "Not yet implemented"})

(defn bootstrap [opts]
  (boot-on-container handler (conj {:host "127.0.0.1" :path "/admin" :port 3000} opts)))



