(ns bottify.watson.conversation
  (:require [clojure.string :as str]
            [clojure.data.json :as json]
            [org.httpkit.client :as http]))

(def ^:dynamic *context* {})

(defn- workspaces
  ([name]
   (filter #(= name (:name %)) (:workspaces (workspaces))))
  ([]
   (json/read-str
    (:body
     @(http/get (str (:url *context*) "/v1/workspaces" )
                {:basic-auth [(:username *context*) (:password *context*)]
                 :query-params {:version "2016-09-20"}}))
    :key-fn keyword)))

(defn ask [text]
  (let [workspace-id (:workspace_id (first (workspaces "irmagician")))]
    (json/read-str
     (:body
      @(http/post (str (:url *context*) "/v1/workspaces/" workspace-id "/message")
                  {:headers {"Content-Type" "application/json"}
                   :body (json/write-str {:input {:text text}})
                   :basic-auth [(:username *context*) (:password *context*)]
                   :query-params {:version "2016-09-20"}}))
     :key-fn keyword)))


