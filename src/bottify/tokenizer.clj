(ns bottify.tokenizer
  (:require [clojure.set :as s :refer [union]]
            [clojure.string :as str])
  (:require [taoensso.timbre :refer :all])
  (:import  [com.atilika.kuromoji.ipadic Token Tokenizer]))

(defonce tokenizer (Tokenizer.))



(defn parse [txt]
  (let [tokens (map #(cons (.getSurface %) (seq (.getAllFeaturesArray %))) (-> tokenizer (.tokenize txt)))
        nouns (map first (filter #(= (second %) "名詞") tokens))
        verbs (map #(nth % 7) (filter #(= (second %) "動詞") tokens))]
    {:nouns (set nouns)
     :verbs (set verbs)}))

