(ns bottify.dialogflow.agent)

(defn build [json-handler]
  ;; Just return json-handler
  json-handler)

;; {
;;   "responseId": "3eaac471-90cc-4c8f-882c-3e4e9e95cbd2",
;;   "queryResult": {
;;     "queryText": "電気つけて",
;;     "action": "operation",
;;     "parameters": {
;;       "any": "電気つけて",
;;       "location": ""
;;     },
;;     "allRequiredParamsPresent": true,
;;     "fulfillmentText": "はい",
;;     "fulfillmentMessages": [
;;       {
;;         "text": {
;;           "text": [
;;             "はい"
;;           ]
;;         }
;;       }
;;     ],
;;     "intent": {
;;       "name": "projects/newagent-d7023/agent/intents/806fd207-9ebf-4359-b5c8-4542edb4d3f5",
;;       "displayName": "SmartHome"
;;     },
;;     "intentDetectionConfidence": 1,
;;     "diagnosticInfo": {},
;;     "languageCode": "ja"
;;   },
;;   "webhookStatus": {
;;     "code": 3,
;;     "message": "Webhook call failed. Error: 400 Bad Request"
;;   }
;; }
