(ns bottify.alexa.skill
  (:require [taoensso.timbre :refer :all])
  (:require [clojure.string :as str])
  (:import [com.amazon.speech.speechlet Speechlet]
           [com.amazon.speech.speechlet.servlet SpeechletServlet]))

(defn build [skill-handler opts]
  (doto (SpeechletServlet.)
     (.setSpeechlet
      (try
        (skill-handler opts)
        (catch Exception e (error e))))))

;; (defn build [skill-path opts]
;;   (doto (SpeechletServlet.)
;;      (.setSpeechlet
;;       (let [skill-name (str/replace (name skill-path) #"[\./]" ".")]
;;         (binding [*options* opts]
;;           (try
;;             ((ns-resolve
;;               (some  #(when (re-find (re-pattern (str skill-name "$")) (str %)) %) (all-ns))
;;               (symbol "speechlet")) opts)
;;             (catch Exception e (error e))))))))

;; (defmacro build [skill-path opts]
;;   `(doto (SpeechletServlet.)
;;      (.setSpeechlet
;;       (let [skill-name# (str/replace (name ~skill-path) #"[\./]" ".")]
;;         (binding [*options* ~opts]
;;           (try
;;             ((ns-resolve
;;               (some  #(when (re-find (re-pattern (str skill-name# "$")) (str %)) %) (all-ns))
;;               (symbol "speechlet")) ~opts)
;;             (catch Exception e# (error e#))))))))

