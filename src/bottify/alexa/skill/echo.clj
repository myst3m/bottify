(ns bottify.alexa.skill.echo
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.set :as s]
            [bottify.alexa.skill :as skill]
            [bottify.container :refer [boot-on-container]]
            [taoensso.timbre :refer :all])
  (:import [com.amazon.speech.speechlet Speechlet]
           [com.amazon.speech.ui SimpleCard PlainTextOutputSpeech]
           [com.amazon.speech.speechlet SpeechletResponse]
           [com.amazon.speech.speechlet.servlet SpeechletServlet]))

(defn speechlet [opts]
  (proxy [Speechlet][]
    (onSessionStarted [session-started-req session]
      (info session-started-req))
    (onIntent [req session]
      (let [msg  (str "Intent wa " (-> req (.getIntent) (.getName)) " de su ne") ]
        (SpeechletResponse/newTellResponse (doto (PlainTextOutputSpeech. )
                                             (.setText msg))
                                           (doto (SimpleCard.)
                                             (.setTitle "Card")
                                             (.setContent msg)))))))


(defn bootstrap [skill-opts]
  (boot-on-container (skill/build speechlet skill-opts) {:host "0.0.0.0" :path "/echo" :port 10000}))
