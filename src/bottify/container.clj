(ns bottify.container
  (:gen-class)
  (:require [immutant.web :as web]
            [clojure.set :as s]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.tools.cli :refer [parse-opts]]
            [bottify.line.bot :as line]
            [bottify.alexa.skill :as alexa]
            [clojure.data.json :as json]
            [taoensso.timbre :as log]))

(defonce ^:dynamic *container* (atom {}))

(defn container []
  @*container*)

(defn init-default-options [{:keys [log-level]
                             :or {log-level :info}
                             :as opts} ]
  (assoc opts :log-level log-level))


(def default-cli-options
  [["-d" "--log-level LOG-LEVEL" "log level (info)"
    :parse-fn #(keyword (or % "info"))]
   ["-s" "--secrets SECRETS" "File that contains secrets"]
   ["-p" "--port PORT" "Listen port"
    :parse-fn #(Long/parseLong %)
    :default 10000]
   [nil "--host HOST" "Listen host"
    :default "0.0.0.0"]
   [nil "--path PATH" "Path to listen"]
   [nil "--admin" "Start admin application with"
    :default false]
   [nil "--help" "This help"]])


(defn boot-on-container [app options]
  (reset! *container* (web/run app (merge @*container* options))))

(defn boot-on-container* [app options]
  (reset! *container* (web/run-dmc app (merge @*container* options))))

(defn stop-container []
  (web/stop @*container*)
  (swap! *container* empty))

(defn parse-args [args user-cli-options]
  (let [{default-summary :summary}
        (parse-opts args default-cli-options)
        {user-summary :summary}
        (parse-opts args user-cli-options)        
        {:keys [options arguments summary]}
        (parse-opts args (apply conj default-cli-options user-cli-options))]
    {:options (init-default-options options)
     :arguments arguments
     :summary (str "Options:\n" default-summary "\n""\n" "User Options:\n" user-summary)} ))

;; args is expected as main arguments
(defn start-container
  ([args]
   (start-container args []))
  ([args user-cli-options]
   (let [{:keys [options arguments summary]}
         (parse-args args user-cli-options)]

   (if (:help options)
      (println summary)
      (let [{:keys [log-level]} options]  
        (log/set-level! log-level)
        (try
          (doseq [app-path (map symbol arguments)]
            (require app-path)
            (let [app (ns-resolve (the-ns app-path) 'bootstrap)]
              (log/debug options)
              (app options)))
          
          (catch Exception e (do (stop-container)
                                 (log/error (-> e (.getMessage)))
                                 (throw e)))))))))


(defn -main [& args]
  (try
    (start-container args)
    (catch Exception e (System/exit 100))))

(defn echo-bot []
  (-main "--path" "/sofia"
         "bottify.line.bot.echo"
         "-s" "echo.secrets"
         "-p" "8080"
         "-d" "debug"))

(defn echo-skill []
  (-main "--path" "/alissia"
         "bottify.alexa.skill.echo"
         "-d" "debug"
         "-p" "10001"))
