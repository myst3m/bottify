(set-env!
 :source-paths #{"src" "test"}
 :resource-paths #{"src"}
 :dependencies '[[org.clojure/clojure "1.9.0"]
                 [org.clojure/data.json "0.2.6"]
                 [metosin/ring-http-response "0.9.0"]
                 [org.immutant/web "2.1.9"
                  :exclusions [ch.qos.logback/logback-classic]]
                 [boot-immutant "0.6.0" :scope "test"]

                 ;; LINE
                 [com.linecorp.bot/line-bot-model "1.13.0"]
                 [com.linecorp.bot/line-bot-servlet "1.13.0"]
                 [com.linecorp.bot/line-bot-api-client "1.13.0"]

                 ;; Alexa
                 [com.amazon.alexa/alexa-skills-kit "1.8.1"]

                 ;; Watson
                 [http-kit "2.2.0"]

                 
                 [com.taoensso/timbre "4.10.0"]
                 [adzerk/bootlaces "0.1.13" :scope "test"]
                 [adzerk/boot-test "1.2.0" :scope "test"]
                 [com.fzakaria/slf4j-timbre "0.3.8"]
                 [com.atilika.kuromoji/kuromoji-core "0.9.0"]
                 [com.atilika.kuromoji/kuromoji-ipadic "0.9.0"]
                 [org.clojure/tools.cli "0.3.5"]])

(require '[adzerk.boot-test :refer :all])
(require '[adzerk.bootlaces :refer :all])

(def +version+ "0.9.3")

(task-options!
 pom {:project 'bottify
      :version +version+})

(deftask build []
  (comp (aot :all true) (pom) (uber)  (jar :main 'bottify.container :file "bottify-" +version+ ".jar") (target)))

