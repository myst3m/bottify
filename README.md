# Bottify

This library provides a set of functions to enable users to create LINE bots easily.

[![Clojars Project](https://img.shields.io/clojars/v/bottify.svg)](https://clojars.org/bottify)

## Installation

Download the files and boot-clj (https://github.com/boot-clj).
execute the following command to generate uberjar.
```sh
boot build 
```

For library, execute the following command.
```sh
boot build-jar 
```

## Documents
Still under development.

## License

Copyright © 2017-2018 Tsutomu Miyashita.

Distributed under the Eclipse Public License either version 1.0 or any later version.
# bottify
